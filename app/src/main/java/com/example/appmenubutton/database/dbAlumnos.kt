package com.example.appmenubutton.database

import android.content.Context
import android.content.LocusId
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.core.content.contentValuesOf
import androidx.core.graphics.Insets

@Suppress("NAME_SHADOWING", "UNREACHABLE_CODE")
class dbAlumnos (private val context: Context){
    private val dbHelper:AlumnosDbHelper = AlumnosDbHelper(context)
    private lateinit var db:SQLiteDatabase

    private val leerCampos = arrayOf(
        DefinirTabla.Alumnos.ID,
        DefinirTabla.Alumnos.MATRICULA,
        DefinirTabla.Alumnos.NOMBRE,
        DefinirTabla.Alumnos.DOMICILIO,
        DefinirTabla.Alumnos.ESPECIALIDAD,
        DefinirTabla.Alumnos.FOTO
    )

    fun openDataBase(){
        db = dbHelper.writableDatabase
    }

    fun InsertarAlumno(alumno: Alumno): Long{
        val valores = contentValuesOf().apply {
            put(DefinirTabla.Alumnos.MATRICULA,alumno.matricula)
            put(DefinirTabla.Alumnos.NOMBRE,alumno.nombre)
            put(DefinirTabla.Alumnos.DOMICILIO,alumno.domicilio)
            put(DefinirTabla.Alumnos.ESPECIALIDAD,alumno.especialidad)
            put(DefinirTabla.Alumnos.FOTO,alumno.foto)

        }

        return db.insert(DefinirTabla.Alumnos.TABLA,null,valores)
    }

    fun ActualizarAlumno(alumno: Alumno){
        val valores = contentValuesOf().apply {
            put(DefinirTabla.Alumnos.MATRICULA,alumno.matricula)
            put(DefinirTabla.Alumnos.NOMBRE,alumno.nombre)
            put(DefinirTabla.Alumnos.DOMICILIO,alumno.domicilio)
            put(DefinirTabla.Alumnos.ESPECIALIDAD,alumno.especialidad)
            put(DefinirTabla.Alumnos.FOTO,alumno.foto)

        }

        db.update(DefinirTabla.Alumnos.TABLA,valores,
            "${DefinirTabla.Alumnos.MATRICULA} = ?",arrayOf(alumno.matricula))
    }
    fun BorrarAlumno(matricula: String):String{
        return db.delete(DefinirTabla.Alumnos.TABLA,
            "${DefinirTabla.Alumnos.MATRICULA} = ?", arrayOf(matricula.toString())).toString()
    }
    fun mostrarAlumnos(cursor: Cursor): Alumno {
        return Alumno().apply {
            id = cursor.getInt(0)
            matricula = cursor.getString(1)
            nombre = cursor.getString(2)
            domicilio = cursor.getString(3)
            especialidad = cursor.getString(4)
            foto = cursor.getString(5)
        }
    }
    fun getAlumno(matricula : String):Alumno{
        val db = dbHelper.readableDatabase
        val cursor = db.query(DefinirTabla.Alumnos.TABLA,leerCampos,"${DefinirTabla.Alumnos.MATRICULA} = ?",
                arrayOf(matricula.toString() ),null,null,null)

        if(cursor.moveToFirst()){
            val alumno = mostrarAlumnos(cursor)
            cursor.close()
            return alumno
        }
        else{
            cursor.close()
            var alumno: Alumno = Alumno()
            return alumno
        }
    }

    fun getAllAlumnos(): Cursor {
        val db = dbHelper.readableDatabase
        return db.query(DefinirTabla.Alumnos.TABLA, leerCampos, null, null, null, null, null)
    }


    fun leerTodos(): ArrayList<Alumno> {
        val listaAlumno = ArrayList<Alumno>()
        val cursor = db.query(
            DefinirTabla.Alumnos.TABLA,
            leerCampos,  // Asegúrate de que leerCampos es un array de las columnas necesarias
            null, null, null, null, null
        )

        cursor?.use {
            if (it.moveToFirst()) {
                do {
                    val alumno = mostrarAlumnos(it)
                    listaAlumno.add(alumno)
                } while (it.moveToNext())
            }
        }
        cursor.close()
        return listaAlumno
    }

    fun close() {
        dbHelper.close()
    }
}


