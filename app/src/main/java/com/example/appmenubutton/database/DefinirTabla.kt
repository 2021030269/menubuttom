package com.example.appmenubutton.database

import android.provider.BaseColumns

class DefinirTabla {
    // Clase estatica: No genera objetos, hace referencia con el nombre de la clase y los atributos
    object Alumnos:BaseColumns{
        const val TABLA = "alumnos"
        const val ID = "id"
        const val MATRICULA = "matricula"
        const val NOMBRE = "nombre"
        const val DOMICILIO = "domicilio"
        const val ESPECIALIDAD = "especialidad"
        const val FOTO = "foto"
    }
}