package com.example.appmenubutton

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubutton.database.dbAlumnos
import com.google.android.material.floatingactionbutton.FloatingActionButton

class AcercaFragment : Fragment() {
    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var btnNuevo: FloatingActionButton
    private lateinit var db: dbAlumnos
    private lateinit var listaAlumno: ArrayList<AlumnoLista>
    private lateinit var searchView: SearchView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_acercade, container, false)
        rcvLista = view.findViewById(R.id.recId)
        btnNuevo = view.findViewById(R.id.agregarAlumno)
        searchView = view.findViewById(R.id.searchView)
        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        db = dbAlumnos(requireContext())
        db.openDataBase()
        Log.d("AcercaFragment", "Database opened successfully")
        // Obtener la lista de alumnos desde la base de datos

        val listaAlumno = try {
            db.leerTodos().map { alumno ->
                AlumnoLista(
                    id = alumno.id,
                    matricula = alumno.matricula,
                    nombre = alumno.nombre,
                    domicilio = alumno.domicilio,
                    especialidad = alumno.especialidad,
                    foto = alumno.foto  // Esto ahora es un String que puede ser una URI o un resource ID
                ).also {
                    Log.e("AcercaFragment", "Alumno: ${it.nombre}, foto: ${it.foto}")
                }
            }
        } catch (e: Exception) {
            Log.e("AcercaFragment", "Error reading database", e)
            emptyList<AlumnoLista>()
        }

        Log.d("AcercaFragment", "Alumnos retrieved: ${listaAlumno.size}")

        adaptador = MiAdaptador(ArrayList(listaAlumno), requireContext())
        rcvLista.adapter = adaptador

        btnNuevo.setOnClickListener {
            cambiarDBFragment()
        }

        adaptador.setOnClickListener({
            val pos: Int = rcvLista.getChildAdapterPosition(it)

            //Obtener el objeto
            val alumno: AlumnoLista
            alumno = listaAlumno[pos]
            Log.d("AcercaFragment", "Clicked on: ${alumno.nombre}, Foto: ${alumno.foto}")
            //agrgar el objeto
            val bundle = Bundle().apply {

                putSerializable("mialumno", alumno)
            }

            //asignar al fragments
            val dbFragment = dbFragment()
            dbFragment.arguments = bundle
            /*val dbFragment = dbFragment().apply {
                arguments = bundle
            }*/
            //llamar al fragments destino
            parentFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .addToBackStack(null)
                .commit()
        })

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adaptador.filtrar(newText.orEmpty())
                return true
            }
        })




        return view
    }

    private fun cambiarDBFragment() {
        val cambioFragment = fragmentManager?.beginTransaction()
        cambioFragment?.replace(R.id.frmContenedor, dbFragment())
        cambioFragment?.addToBackStack(null)
        cambioFragment?.commit()
    }

}