package com.example.appmenubutton

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class MiAdaptador(
    private var listaAlumnos: ArrayList<AlumnoLista>,
    private val context: Context
) : RecyclerView.Adapter<MiAdaptador.ViewHolder>(){
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var listener: View.OnClickListener? = null
    private var listaAlumnosOriginal: ArrayList<AlumnoLista> = ArrayList(listaAlumnos)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.alumno_item, parent, false)
        return ViewHolder(view).apply {
            itemView.setOnClickListener{
                listener?.onClick(it)
            }
        }
        /*view.setOnClickListener(this)
        return ViewHolder(view)*/
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val alumno = listaAlumnos[position]
        holder.txtMatricula.text = alumno.matricula
        holder.txtNombre.text = alumno.nombre
        holder.txtCarrera.text = alumno.especialidad

        // Cargar la imagen usando Glide
        Glide.with(context)
            .load(alumno.foto)
            .placeholder(R.drawable.subir) // Placeholder en caso de que no se cargue la imagen
            .into(holder.idImagen)
    }

    override fun getItemCount(): Int {
        return listaAlumnos.size
    }

    fun setOnClickListener(listener: View.OnClickListener) {
        this.listener = listener
    }

    fun actualizarLista(lista: List<AlumnoLista>) {
        listaAlumnos = ArrayList(lista)
        listaAlumnosOriginal = ArrayList(lista)
        notifyDataSetChanged()
    }

    fun filtrar(query: String) {
        listaAlumnos = if (query.isEmpty()) {
            listaAlumnosOriginal
        } else {
            val filteredList = ArrayList<AlumnoLista>()
            for (alumno in listaAlumnosOriginal) {
                if (alumno.nombre.contains(query, true) || alumno.matricula.contains(query, true)) {
                    filteredList.add(alumno)
                }
            }
            filteredList
        }
        notifyDataSetChanged()
    }


    /*override fun onClick(v: View) {
        listener?.onClick(v)
    }*/


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtNombre: TextView = itemView.findViewById(R.id.txtAlumnoNombre)
        val txtMatricula: TextView = itemView.findViewById(R.id.txtMatricula)
        val txtCarrera: TextView = itemView.findViewById(R.id.txtCarrera)
        val idImagen: ImageView = itemView.findViewById(R.id.foto) // Cambiado a ImageView


    }
}
